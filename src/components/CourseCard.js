import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){

console.log(courseProp)

//object destructuring
const {name, description, price, _id} = courseProp
// Syntax: const{properties} = propname

//array destructuring
//const [count, setCount] = useState(0)
// Syntax: const [getter, setter] = useState(initialValue)

//Hook used is useState - to store the state

/*function enroll(){
	
	if (count < 30) {
		setCount(count+1);
		console.log('Enrollees' + count)	
	} else {
		alert(`No more seats`)
	}
}*/

	return (
		 <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className = "btn btn-primary" to ={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>

	)
}
