import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/*const name = 'Jan Smith'

const user = {
  firstName: 'Solar',
  lastName: 'Moo'
}

function formatName(user) {
  return user.firstName + ' ' + user.lastName
}

const element = <h1>Hello, {formatName(user)}</h1>

ReactDOM.render(
    element,
    document.getElementById('root')
)*/

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals