import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Fragment} from 'react'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register() {

	const {user, setUser} = useContext(UserContext)

	// State hooks to store the values of the input fields.
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	

	// State to determine whether register button is enabled or not.
	const [isActive, setIsActive] = useState(false)

	console.log(email)
	console.log(password1)
	console.log(password2)

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		}).then (res => res.json())
		.then(data => {
			if (data === true) {
				Swal.fire({
					title: 'Duplicate email found.',
					icon: 'error',
					text: 'Please provide a different email'
				})
			} else {

			fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNumber
				})
			})
			.then(res => res.json ())
			.then (data => {
				if (data === true) {
					Swal.fire({
						title: 'Registration successful!',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					}).then(redirect => {
						window.location = "/login"
					})		
				}
			})

			}
		})

				
		//Clears the input fields
		setEmail('')
		setPassword1('')
		setPassword2('')
		setFirstName('')
		setLastName('')
		setMobileNumber('')

	}

	useEffect(() => {
		if((email !== '') && (password1 !== '') && (password2 !== '') && (firstName !== '') && (lastName !== '') && (mobileNumber !== '') && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNumber])

	return (
		(user.id !== null) ? 
		<Redirect to = "/courses" /> : 
		<Fragment>
		<h1>Register</h1>
		<Form onSubmit={(e)=> registerUser(e)}>
			
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type= 'text'	
					value = {firstName}
					onChange = {e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type= 'text'	
					value = {lastName}
					onChange = {e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>	

			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type= 'email'	
					placeholder= 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className = "text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type= 'string'	
					value = {mobileNumber}
					onChange = {e => setMobileNumber(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type= 'password'
					placeholder = 'Please input your password here'
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId = "password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
 					type = 'password'
 					placeholder = 'Please verify your password'
 					value = {password2}
 					onChange = {e => setPassword2(e.target.value)}
 					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'> 
					Register 
				</Button> :
				<Button variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
					Register
				</Button>
			}
		</Form>
		</Fragment>
	)
}

//Ternary operator with the button tag
	// ? - if (If button isActive, the color will be primary and it is enabled)
	// : - else (the color will be red and it is disabled)

// useEffect - react hook
	 /*Syntax: useEffect(() => {
		<conditions>
	}, [parameter])*/

	// once a change happens in the parameter, the use effect will
